<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
    <modelVersion>4.0.0</modelVersion>

    <groupId>cat.inspiracio</groupId>
    <artifactId>rhino-js-engine</artifactId>
    <version>1.7.14</version>
	<packaging>jar</packaging>
    
    <name>rhino-js-engine</name>
    <url>http://www.inspiracio.cat</url>

	<description>
A js-engine.jar that provides a script engine "rhino" with old Rhino JavaScript.

The source code for js-engine comes from https://java.net/projects/Scripting.

The Rhino engine itself is pulled by maven. Its source is at https://github.com/mozilla/rhino.
	</description>

	<developers>
		<developer>
			<name>Alexander Bunkenburg</name>
			<email>alex@inspiracio.cat</email>
			<organization>inspiració.cat</organization>
			<organizationUrl>http://www.inspiracio.cat</organizationUrl>
		</developer>
	</developers>

    <licenses>
        <license>
            <name>The BSD 3-Clause License</name>
            <url>https://opensource.org/licenses/BSD-3-Clause</url>
        </license>
    </licenses>

	<!-- bitbucket -->
	<scm>
		<!-- read-only connection -->
		<connection>scm:git:git@bitbucket.org:bunkenburg/rhino-js-engine.git</connection>
		<!-- read and write access connection -->
		<developerConnection>scm:git:git@bitbucket.org:bunkenburg/rhino-js-engine.git</developerConnection>
		<!-- web front end -->
		<url>https://bitbucket.org/bunkenburg/rhino-js-engine</url>
	</scm>

	<properties>
        <project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>
        <maven.compiler.showDeprecation>true</maven.compiler.showDeprecation>
		<maven.compiler.source>1.8</maven.compiler.source>
		<maven.compiler.target>1.8</maven.compiler.target>
    </properties>

	<distributionManagement>
		<snapshotRepository>
			<id>ossrh</id>
			<url>https://oss.sonatype.org/content/repositories/snapshots</url>
		</snapshotRepository>
	</distributionManagement>

	<build>
		<plugins>

			<!-- clean -->
			<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-clean-plugin</artifactId>
				<version>3.3.2</version>
			</plugin>

			<!-- versions -->
			<plugin>
				<groupId>org.codehaus.mojo</groupId>
				<artifactId>versions-maven-plugin</artifactId>
				<version>2.16.2</version>
				<executions>
					<execution>
						<phase>validate</phase>
						<goals>
							<goal>display-dependency-updates</goal>
							<goal>display-plugin-updates</goal>
						</goals>
					</execution>
				</executions>
			</plugin>

			<!-- enforces java version and maven version -->
			<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-enforcer-plugin</artifactId>
				<version>3.4.1</version>
				<executions>
					<execution>
						<id>enforce-versions</id>
						<phase>validate</phase>
						<goals><goal>enforce</goal></goals>
						<configuration>
							<rules>
								<!-- min maven version -->
								<requireMavenVersion><version>3.6.3</version></requireMavenVersion>
								<!-- exactly java 1.8 -->
								<requireJavaVersion><version>1.8</version></requireJavaVersion>
							</rules>
						</configuration>
					</execution>
				</executions>
			</plugin>

			<!-- compile -->
			<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-compiler-plugin</artifactId>
				<version>3.13.0</version>
				<configuration>
					<debug>true</debug>
					<debuglevel>lines,vars,source</debuglevel>
				</configuration>
			</plugin>

       		<!-- nexus -->
			<plugin>
				<groupId>org.sonatype.plugins</groupId>
				<artifactId>nexus-staging-maven-plugin</artifactId>
				<version>1.6.13</version>
				<extensions>true</extensions>
				<configuration>
					<serverId>ossrh</serverId>
					<nexusUrl>https://oss.sonatype.org/</nexusUrl>
					<autoReleaseAfterClose>true</autoReleaseAfterClose>
				</configuration>
			</plugin>

			<!-- source.jar -->
			<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-source-plugin</artifactId>
				<version>3.3.1</version>
				<executions>
					<execution>
						<id>attach-sources</id>
						<goals><goal>jar</goal></goals>
					</execution>
				</executions>
			</plugin>
    
			<!-- javadoc.jar -->
			<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-javadoc-plugin</artifactId>
				<version>3.6.3</version>
				<executions>
					<execution>
						<id>attach-javadocs</id>
						<goals><goal>jar</goal></goals>
					</execution>
				</executions>
				<configuration>
					<failOnError>false</failOnError>
				</configuration>
			</plugin>

			<!-- gpg signing -->
			<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-gpg-plugin</artifactId>
				<version>3.2.4</version>
				<executions>
					<execution>
						<id>sign-artifacts</id>
						<phase>verify</phase>
						<goals><goal>sign</goal></goals>
					</execution>
				</executions>
			</plugin>

			<!-- deploy -->
			<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-deploy-plugin</artifactId>
				<version>3.1.2</version>
			</plugin>

			<!-- install -->
			<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-install-plugin</artifactId>
				<version>3.1.2</version>
			</plugin>

			<!-- jar -->
			<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-jar-plugin</artifactId>
				<version>3.4.1</version>
			</plugin>

			<!-- resources -->
			<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-resources-plugin</artifactId>
				<version>3.3.1</version>
			</plugin>

			<!-- site -->
			<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-site-plugin</artifactId>
				<version>4.0.0-M13</version>
			</plugin>

			<!-- surefire -->
			<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-surefire-plugin</artifactId>
				<version>3.2.5</version>
			</plugin>

		</plugins>
	</build>
	
    <dependencies>

		<!-- Rhino JavaScript engine (On GAE, there is no JavaScript engine.) -->
		<dependency>
		    <groupId>org.mozilla</groupId>
		    <artifactId>rhino</artifactId>
		    <version>${project.version}</version>
		</dependency>

		<!-- junit -->
		<dependency>
			<groupId>junit</groupId>
			<artifactId>junit</artifactId>
			<version>4.13.2</version>
			<scope>test</scope>
		</dependency>

    </dependencies>

</project>