package cat.inspiracio.script;

import static org.junit.Assert.assertTrue;

import org.junit.Assert;

abstract class AbstractTest {

	/** Asserts that an object is an instance of a class.
	 * @param c
	 * @param o */
	protected void assertInstance(Class<?> c, Object o) {
		assertTrue(c.isInstance(o));
	}

	protected String version(){return System.getProperty("java.version");}

	/** Assert the version of the JVM. 
	 * @param v A prefix of the desired JVM version, like "1.8". */
	protected void assertVersion(String v) {
		String version=version();//1.7.0_95 1.8.0_66-internal
		Assert.assertTrue(version, version.startsWith(v));
	}

}
